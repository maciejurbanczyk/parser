using System;
using System.Text;
using NUnit.Framework;
using Parser.Providers;

namespace Parser.Tests
{
    public class ParserTests
    {
        private IParser _stockParser;
        private StockStatusParserConfig _config;

        [SetUp]
        public void Setup()
        {
            //Arrange
            _config = new StockStatusParserConfig("#", ";", "|", ",");
        }

        [Test]
        public void Parser_should_ignore_hash_line()
        {
            //Arrange
            var input = "# Material inventory initial state as of Jan 01 2018";
            _stockParser = new StockStatusParser(_config);

            //Act
            var result = _stockParser.Parse(input);

            //Assert
            Assert.IsEmpty(result);
        }

        [Test]
        public void Parser_should_parse_one_stock_with_product()
        {
            //Arrange
            var input = new StringBuilder();
            input.Append("# Material inventory initial state as of Jan 01 2018");
            input.AppendLine();
            input.Append("Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5");

            _stockParser = new StockStatusParser(_config);

            //Act
            var result = _stockParser.Parse(input.ToString());

            //Assert
            Assert.AreEqual($"WH-A (total 5){Environment.NewLine}COM-100001: 5", result);
        }

        [Test]
        public void Parser_should_parse_one_stock_with_product2()
        {
            //Arrange
            var input = new DefaultInputProvider().Provide();

            _stockParser = new StockStatusParser(_config);

            //Act
            _stockParser.Parse(input);

            //Assert
            Assert.Pass();
        }
    }
}