using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Parser.Models;

namespace Parser.Tests
{
    public class StockStatusSorertTests
    {
        [Test]
        public void Parser_should_parse_one_stock_with_product12()
        {
            //Arrange
            var items = new List<StockStatus>
            {
                new StockStatus(10, new Product("3M"), new Stock("WH-A")),
                new StockStatus(5, new Product("COM-10"), new Stock("WH-A")),
                new StockStatus(10, new Product("COM-12"), new Stock("WH-A")),

                new StockStatus(13, new Product("CB0"), new Stock("WH-C")),
                new StockStatus(8, new Product("COM-10"), new Stock("WH-C")),
                new StockStatus(10, new Product("COM-12"), new Stock("WH-C")),

                new StockStatus(1, new Product("3M"), new Stock("WH-B")),
                new StockStatus(5, new Product("CB0"), new Stock("WH-B")),
                new StockStatus(10, new Product("COM-10"), new Stock("WH-B"))
            };

            var sorter = new StockStatusSorter();

            //Act
            var result = sorter.Sort(items);

            //Assert
            Assert.AreEqual("WH-C", result.First().Stock.Name);
        }
    }
}