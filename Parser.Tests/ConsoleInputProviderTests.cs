using System;
using Moq;
using NUnit.Framework;
using Parser.Providers;
using Parser.Wrappers;

namespace Parser.Tests
{
    public class ConsoleInputProviderTests
    {
        private IInputProvider _inputProvider;
        private Mock<IConsoleWrapper> _consoleMock;

        [SetUp]
        public void Setup()
        {
            //Arrange
            _consoleMock = new Mock<IConsoleWrapper>();
        }

        [Test]
        public void Provider_should_return_empty_string()
        {
            //Arrange
            _consoleMock
                .Setup(x => x.Read())
                .Returns(string.Empty);

            _inputProvider = new ConsoleInputProvider(_consoleMock.Object);

            //Act
            var result = _inputProvider.Provide();
            
            //Assert
            Assert.NotNull(result);
            Assert.IsEmpty(result);
        }

        [Test]
        public void Provider_should_return_single_line_string()
        {
            //Arrange
            _consoleMock
                .SetupSequence(x => x.Read())
                .Returns("test string")
                .Returns(string.Empty);

            _inputProvider = new ConsoleInputProvider(_consoleMock.Object);

            //Act
            var result = _inputProvider.Provide();
            var lines = result.Split(Environment.NewLine).Length;

            //Assert
            Assert.NotNull(result);
            Assert.AreEqual(1, lines);
        }


        [Test]
        public void Provider_should_return_two_lines_string()
        {
            //Arrange
            _consoleMock
                .SetupSequence(x => x.Read())
                .Returns("test string")
                .Returns("second test string")
                .Returns(string.Empty);

            _inputProvider = new ConsoleInputProvider(_consoleMock.Object);

            //Act
            var result = _inputProvider.Provide();
            var lines = result.Split(Environment.NewLine).Length;

            //Assert
            Assert.NotNull(result);
            Assert.AreEqual(2, lines);
        }
    }
}