﻿using System;
using Moq;
using NUnit.Framework;
using Parser.Providers;
using Parser.Wrappers;

namespace Parser.Tests
{
    public class FileInputProviderTests
    {
        private IInputProvider _inputProvider;
        private Mock<IFileWrapper> _fileMock;

        [SetUp]
        public void Setup()
        {
            //Arrange
            _fileMock = new Mock<IFileWrapper>();
        }

        [Test]
        public void Provider_should_return_empty_string()
        {
            //Arrange
            _fileMock
                .Setup(x => x.ReadAllText(It.IsAny<string>()))
                .Returns("");

            _inputProvider = new FileInputProvider(_fileMock.Object, It.IsAny<string>());

            //Act
            var result = _inputProvider.Provide();

            //Assert
            Assert.NotNull(result);
            Assert.IsEmpty(result);
        }

        [Test]
        public void Provider_should_return_two_lines_string()
        {
            //Arrange
            _fileMock
                .Setup(x => x.ReadAllText(It.IsAny<string>()))
                .Returns($"test string{Environment.NewLine}test string");

            _inputProvider = new FileInputProvider(_fileMock.Object, It.IsAny<string>());

            //Act
            var result = _inputProvider.Provide();
            var lines = result.Split(Environment.NewLine).Length;

            //Assert
            Assert.NotNull(result);
            Assert.AreEqual(2, lines);
        }
    }
}
