﻿using System.Collections.Generic;
using System.Linq;
using Parser.Models;

namespace Parser
{
    public class StockStatusSorter : ISorter<StockStatus>
    {
        public IEnumerable<StockStatus> Sort(IEnumerable<StockStatus> items)
        {
            var result = items
                .OrderBy(x => x.Item.Id)
                .GroupBy(x => x.Stock.Name)
                .OrderByDescending(x => x.Sum(s => s.Amount))
                .ThenByDescending(x => x.Key)
                .SelectMany(x => x)
                .ToList();

            return result;
        }
    }
}
