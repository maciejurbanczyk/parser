﻿namespace Parser.Models
{
    public class Product
    {
        public Product(string id)
        {
            Id = id;
        }

        public Product(string id, string name) : this(id)
        {
            Name = name;
        }

        public string Name { get; }
        public string Id { get; }
    }
}
