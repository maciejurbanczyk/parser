﻿namespace Parser.Models
{
    public class StockStatus
    {
        public StockStatus(decimal amount, Product item, Stock stock)
        {
            Amount = amount;
            Item = item;
            Stock = stock;
        }

        public Stock Stock { get; }
        public Product Item { get; }
        public decimal Amount { get; }
    }
}
