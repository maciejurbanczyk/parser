﻿namespace Parser.Models
{
    public class Stock
    {
        public Stock(string name)
        {
            Name = name;
        }

        public string Name { get; }

    }
}
