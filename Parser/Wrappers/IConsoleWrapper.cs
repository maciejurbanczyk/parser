﻿namespace Parser.Wrappers
{

    public interface IConsoleWrapper
    {
        void WritePrompt(string text = null);
        void WriteLine(string text = null);
        string Read();
    }
}
