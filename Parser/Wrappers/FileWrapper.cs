﻿using System.IO;

namespace Parser.Wrappers
{
    public class FileWrapper : IFileWrapper
    {
        private readonly IConsoleWrapper _console;
        public FileWrapper(IConsoleWrapper console)
        {
            _console = console;
        }

        public string ReadAllText(string filePath)
        {
            try
            {
                var text = File.ReadAllText(filePath);
                return text;
            }
            catch(IOException exception)
            {
                _console.WriteLine("The file could not be read:");
                _console.WriteLine(exception.Message);

                return null;
            }
        }
    }
}
