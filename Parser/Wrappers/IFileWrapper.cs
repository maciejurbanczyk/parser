﻿namespace Parser.Wrappers
{
    public interface IFileWrapper
    {
        string ReadAllText(string filePath);
    }
}
