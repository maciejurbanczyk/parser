﻿namespace Parser
{
    public class StockStatusParserConfig
    {
        public StockStatusParserConfig(string ignoreLineToken, string separatorToken)
        {
            IgnoreLineToken = ignoreLineToken;
            SeparatorToken = separatorToken;
        }

        public StockStatusParserConfig(string ignoreLineToken, string separatorToken, string secondSeparatorToken) :
            this(ignoreLineToken, separatorToken)
        {
            SecondSeparatorToken = secondSeparatorToken;
        }

        public StockStatusParserConfig(string ignoreLineToken, string separatorToken, string secondSeparatorToken, string thirdSeparatorToken) :
            this(ignoreLineToken, separatorToken, secondSeparatorToken)
        {
            ThirdSeparatorToken = thirdSeparatorToken;
        }

        public string IgnoreLineToken { get; }
        public string SeparatorToken { get; }
        public string SecondSeparatorToken { get; }
        public string ThirdSeparatorToken { get; }

    }
}
