﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Parser.Models;

namespace Parser
{
    public class StockStatusParser : IParser
    {
        private StockStatusParserConfig StockStatusParserConfig { get; }
        
        private readonly ISorter<StockStatus> _sorter;

        public StockStatusParser(StockStatusParserConfig stockStatusParserConfig)
        {
            StockStatusParserConfig = stockStatusParserConfig;
        }

        public StockStatusParser(StockStatusParserConfig stockStatusParserConfig, ISorter<StockStatus> sorter) : this(stockStatusParserConfig)
        {
            _sorter = sorter;
        }

        public string Parse(string input)
        {
            var result = new List<StockStatus>();

            foreach (var line in input.ReadLines())
            {
                if (line.StartsWith(StockStatusParserConfig.IgnoreLineToken))
                    continue;

                var lineGroups = line.Split(StockStatusParserConfig.SeparatorToken);

                var productName = lineGroups[(int)StockStatusMapOrder.ProductName];
                var productId = lineGroups[(int)StockStatusMapOrder.ProductId];
                var statuses = lineGroups[(int)StockStatusMapOrder.Statuses].Split(StockStatusParserConfig.SecondSeparatorToken);

                foreach (var status in statuses)
                {
                    var stockStatus = status.Split(StockStatusParserConfig.ThirdSeparatorToken);
                    var stockName = stockStatus[(int) StockStatusMapOrder.StockName];
                    var amount = stockStatus[(int)StockStatusMapOrder.Amount];

                    var parsedStockStatus = new StockStatus(int.Parse(amount), 
                        new Product(productId, productName),
                        new Stock(stockName));

                    result.Add(parsedStockStatus);
                }
            }
            
            if (_sorter != null)
                result = _sorter.Sort(result).ToList();

            var groupedStocks = result.GroupBy(x => x.Stock.Name);
            var output = new StringBuilder();

            foreach (var stock in groupedStocks)
            {
                output.Append($"{stock.Key} (total {stock.Sum(x => x.Amount)})");
                foreach (var stockStatus in stock)
                {
                    output.AppendLine();
                    output.Append($"{stockStatus.Item.Id}: {stockStatus.Amount}");
                }

                if (stock.Key != groupedStocks.Last().Key)
                {
                    output.AppendLine();
                    output.AppendLine();
                }
            }

            return output.ToString();
        }
    }

    public enum StockStatusMapOrder
    {
        ProductName = 0,
        ProductId = 1,
        Statuses = 2,
        StockName = 0,
        Amount = 1
    }
}
