﻿using System.Collections.Generic;

namespace Parser
{

    public interface ISorter<T>
    {
        IEnumerable<T> Sort(IEnumerable<T> items);
    }
}
