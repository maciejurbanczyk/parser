﻿using System.Text;
using Parser.Wrappers;

namespace Parser.Providers
{
    public class ConsoleInputProvider : IInputProvider
    {

        private readonly IConsoleWrapper _console;

        public ConsoleInputProvider(IConsoleWrapper console)
        {
            _console = console;
        }

        public string Provide()
        {
            _console.WritePrompt("Enter empty line to finish string");
            _console.WriteLine();

            var lines = new StringBuilder();
            string line;

            do
            {
                _console.WritePrompt();
                line = _console.Read();

                if (string.IsNullOrEmpty(line))
                    continue;
                if(lines.Length > 0)
                    lines.AppendLine();

                lines.Append(line);
            } while (string.IsNullOrEmpty(line) == false);

            return lines.ToString();
        }
    }
}
