﻿using System;
using Parser.Wrappers;

namespace Parser.Providers
{
    public class ParserConsole : IConsoleWrapper
    {
        private const string Prompt = "provider> ";

        public void WritePrompt(string text = null)
        {
            Console.Write(Prompt + text);
        }

        public void WriteLine(string text = null)
        {
            Console.WriteLine(text);
        }

        public string Read()
        {
            return Console.ReadLine();
        }
    }
}
