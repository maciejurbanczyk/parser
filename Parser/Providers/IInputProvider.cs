﻿namespace Parser.Providers
{
    public interface IInputProvider
    {
        string Provide();
    }
}
