﻿using System.Text;

namespace Parser.Providers
{
    public class DefaultInputProvider : IInputProvider
    {
        public string Provide()
        {
            var input = new StringBuilder();
            input.Append("# Material inventory initial state as of Jan 01 2018");
            input.AppendLine();
            input.Append("# New materials");
            input.AppendLine();
            input.Append("Cherry Hardwood Arched Door - PS;COM-100001;WH-A,5|WH-B,10");
            input.AppendLine();
            input.Append("Maple Dovetail Drawerbox;COM-124047;WH-A,15");
            input.AppendLine();
            input.Append("Generic Wire Pull;COM-123906c;WH-A,10|WH-B,6|WH-C,2");
            input.AppendLine();
            input.Append("Yankee Hardware 110 Deg. Hinge;COM-123908;WH-A,10|WH-B,11");
            input.AppendLine();
            input.Append("# Existing materials, restocked");
            input.AppendLine();
            input.Append("Hdw Accuride CB0115-CASSRC - Locking Handle Kit - Black;CB0115-CASSRC;WH-C,13|WH-B,5");
            input.AppendLine();
            input.Append("Veneer - Charter Industries - 3M Adhesive Backed - Cherry 10mm - Paper Back;3M-Cherry-10mm;WH-A,10|WH-B,1");
            input.AppendLine();
            input.Append("Veneer - Cherry Rotary 1 FSC;COM-123823;WH-C,10");
            input.AppendLine();
            input.Append(@"MDF, CARB2, 1 1/8\"";COM-101734;WH-C,8");

            return input.ToString();
        }
    }
}
