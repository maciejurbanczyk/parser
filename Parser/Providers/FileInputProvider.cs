﻿using Parser.Wrappers;

namespace Parser.Providers
{
    public class FileInputProvider : IInputProvider
    {
        private readonly IFileWrapper _file;

        private string FilePath { get; }

        public FileInputProvider(IFileWrapper file, string filePath)
        {
            _file = file;
            FilePath = filePath;
        }

        public string Provide()
        {
            return _file.ReadAllText(FilePath);
        }
    }
}
