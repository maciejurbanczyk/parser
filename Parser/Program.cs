﻿using System;
using Parser.Models;
using Parser.Providers;

namespace Parser
{
    class Program
    {
        static void Main(string[] args)
        {
            IInputProvider inputProvider = new DefaultInputProvider(); 
            var input = inputProvider.Provide();

            Console.WriteLine(input);
            Console.WriteLine();

            var parserConfig = new StockStatusParserConfig("#", ";", "|", ",");
            ISorter<StockStatus> sorter = new StockStatusSorter();
            var parser = new StockStatusParser(parserConfig, sorter);

            Console.Write(parser.Parse(input));
            Console.WriteLine();
        }
    }
}
