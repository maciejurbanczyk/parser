﻿namespace Parser
{
    public interface IParser
    {
        string Parse(string input);
    }
}
